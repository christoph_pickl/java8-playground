# java8-playground

Just in order to get familiar with the new features introduced with Java 8:

* Lambda Expressions, Functional Interfaces
* Streams and Pipelines
* Async (Futures, Concurrent Accumulators, Parallel operations)
* Optional
* Date and Time API
* Default and static methods in interfaces
* Method references
* Type Annotations

## Gradle

In order to download the Gradle wrapper, execute the following in the root project directory:

	gradle wrapper --gradle-version 5.2.1

You might want to update the version number, and afterwards change from `bin` to `all` in the properties file.