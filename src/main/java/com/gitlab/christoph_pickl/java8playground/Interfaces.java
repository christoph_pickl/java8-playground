package com.gitlab.christoph_pickl.java8playground;

public class Interfaces {
    public static void main(String[] args) {
        SomeImpl some = new SomeImpl();
        System.out.println("isValid: " + some.isValid());
        System.out.println("isInvalid: " + some.isInvalid());

        System.out.println("Answer: " + WithStatic.getAnswer());

        // lambda's only for @FunctionalInterface
        WithDefault lambda = () -> true;
    }
}

class SomeImpl implements WithDefault, WithStatic {

    @Override
    public boolean isValid() {
        return true;
    }

}

@FunctionalInterface // like @Override optional, but best practice for SAMs
interface WithDefault {
    boolean isValid();

    default boolean isInvalid() {
        return !isValid();
    }
}

interface WithStatic {
    static int getAnswer() {
        return 42;
    }
}
