package com.gitlab.christoph_pickl.java8playground;

import java.util.LinkedList;
import java.util.List;

public class Collection {
    public static void main(String[] args) {
        System.out.println("Hello Java 8!");
        forEach();
    }

    private static void forEach() {
        List<Integer> ints = new LinkedList<>();
        ints.add(1);
        ints.add(2);
        ints.add(3);

        // defined by Iterable interface with a default method
        ints.forEach(i -> System.out.println("Int: " + i));
    }
}
