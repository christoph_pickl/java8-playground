package com.gitlab.christoph_pickl.java8playground;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class Stream {
    public static void main(String[] args) {
//        intStream();
        List<StreamObj> objs = Arrays.asList(1, 2, 3).stream()
                                     .collect(Collectors.toMap(i -> i, i -> new StreamObj()))
                                     .entrySet().stream()
                                     .map(entry -> entry.getValue().setId(entry.getKey()))
                                     .collect(Collectors.toList());
        objs.stream()
            .filter(StreamObj::filter)
            .map(StreamObj::transWarp)
//            .collect(Collectors.toList()); ==> invokes methods for all 3 entries!
            .findFirst().ifPresent(obj -> obj.process()); // ==> invokes only for 1 entry
    }

    static void intStream() {
        List<Integer> ints = new LinkedList<Integer>() {{
            add(1);
            add(2);
            add(3);
        }};

        ints.stream().filter(i -> i % 2 == 0)
            .map(i -> i * 2)
            .forEach(i -> System.out.println("Int: " + i)); // prints "Int: 4"
    }
}

class StreamObj {
    private int id;

    StreamObj setId(int id) {
        this.id = id;
        return this;
    }

    boolean filter() {
        System.out.println("filter: " + id);
        return true;
    }

    void process() {
        System.out.println("process: " + id);
    }

    StreamObj transWarp() {
        System.out.println("transWarp: " + id);
        return this;
    }
}
