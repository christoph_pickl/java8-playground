package com.gitlab.christoph_pickl.java8playground;

public class MultiInterfaces {
    public static void main(String[] args) {
        new Implementor().method();
    }
}

interface Interface1 {
    default void method() {
        System.out.println("i1");
    }
}

interface Interface2 {
    default void method() {
        System.out.println("i2");
    }
}

class Implementor implements Interface1, Interface2 {

    // must override, otherwise compile error
    @Override
    public void method() {
        System.out.println("implementor");
    }
}