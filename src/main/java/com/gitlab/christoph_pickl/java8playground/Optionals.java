package com.gitlab.christoph_pickl.java8playground;

import java.util.Optional;

public class Optionals {
    public static void main(String[] args) {
        System.out.println( Optional.empty()); // "Optional.empty"
        System.out.println("optional: " + Optional.of("a")); // "Optional[a]"

        Optional<String> optional = Optional.of("1");

        // Optional.of(null) throws NPE
        Optional.ofNullable(null);

        // throws NPE if null (== empty())
        optional.get();

        // conditional execution
        optional.ifPresent(opt -> System.out.println("Opt is definitely not null: " + opt));
        // simply checks for: value != null
        boolean present = optional.isPresent();

        optional
                // .filter() ignores null values
                .filter(opt -> !opt.isEmpty())
                // .map() ignores null values, uses ofNullable
                .map(opt -> opt.toUpperCase())
                // .flatMap returns already an Optional; .map does it internally
                .flatMap(opt -> Optional.ofNullable(opt.toLowerCase()))
//        .orElse("default")
//        .orElseGet(() -> "heavy lazy computation")
//        .orElseThrow(() -> new RuntimeException())
        ;

    }
}
